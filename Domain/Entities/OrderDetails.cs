﻿namespace Domain.Entities
{
    public class OrderDetails:BaseEntity
    {
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public Orders Orders { get; set; }
        public Products Products { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
