﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Wallet:BaseEntity
    {
        public WalletStatusEnum WalletStatus { get; set; }
        public decimal Remainder { get; set; }
        public Card Card { get; set; }
        public Guid CardId { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
