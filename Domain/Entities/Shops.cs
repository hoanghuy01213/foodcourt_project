﻿namespace Domain.Entities
{
    public class Shops:BaseEntity
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ShopName { get; set; }
        public string Location { get; set; }
        public ICollection<ShopCategory> ShopCategories { get; set; }
    }
}
