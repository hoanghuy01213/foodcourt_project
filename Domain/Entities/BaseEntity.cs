﻿
namespace Domain.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
