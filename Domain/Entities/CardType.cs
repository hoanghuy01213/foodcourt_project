﻿namespace Domain.Entities
{
    public class CardType:BaseEntity
    {
        public string Avatar { get; set; }
        public decimal Points { get; set; }
        public decimal Discount { get; set; }
        public ICollection<Card> Cards { get; set; }
    }
}
