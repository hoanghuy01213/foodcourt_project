﻿namespace Domain.Entities
{
    public class ShopCategory:BaseEntity
    {
        public Guid ShopId { get; set; }
        public Shops Shops { get; set; }
        public string Name { get; set; }
        public ICollection<Products> Products { get; set; }
    }
}
