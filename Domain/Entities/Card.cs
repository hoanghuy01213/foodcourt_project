﻿namespace Domain.Entities
{
    public class Card:BaseEntity
    {
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public decimal points { get; set; }
        public DateTime ExpirationDate { get; set; }
        public ICollection<Orders> Orders { get; set; }
        public ICollection<Wallet> Wallets { get; set; }
        public Guid CardTypeId { get; set; }
        public CardType CardType { get; set; }
    }
}
