﻿namespace Domain.Entities
{
    public class TransactionCounter : BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
