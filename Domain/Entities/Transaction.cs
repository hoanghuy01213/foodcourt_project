﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Transaction:BaseEntity
    {
        public decimal Money { get; set; }
        public TransactionStatusEnum TransactionStatus { get; set; }
        public Guid OrderId { get; set; }
        public Orders Orders { get; set; }
        public Guid? WalletId { get; set; }
        public Wallet Wallet { get; set; }
        public Guid TransactionCounterId { get; set; }
        public TransactionCounter TransactionCounter { get; set; }
    }
}
