﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Orders:BaseEntity
    {
        public OrderStatus OrderStatus { get; set; }
        public Guid CardId{ get; set; }
        public ICollection<OrderDetails> OrderDetails { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
        public Card Card { get; set; }
    }
}
