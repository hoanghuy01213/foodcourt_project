﻿namespace Domain.Entities
{
    public class Products : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string UrlImage { get; set; }
        public Guid ShopCategoryId { get; set; }
        public ShopCategory ShopCategory { get; set; }
        public ICollection<OrderDetails> OrderDetails { get; set; }
    }
}
