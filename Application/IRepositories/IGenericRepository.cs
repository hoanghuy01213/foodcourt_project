﻿using Application.Commons;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetListAsync();
        Task<T?> GetEntityByIdAsync(Guid id);
        Task AddEntityAsync(T entity);
        void UpdateEntity(T entity);
        void UpdateRange(IEnumerable<T> entities);
        void SoftRemoveEntity(T entity);
        Task AddEntityRange(IEnumerable<T> entities);
        void SoftRemoveEntityRange(IEnumerable<T> entities);
        Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10);
    }
}
