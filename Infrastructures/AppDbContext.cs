﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Card> Card { get; set; }
        public DbSet<CardType> CardType { get; set; }
        public DbSet<OrderDetails> OrderDetail { get; set; }
        public DbSet<Orders> Order { get; set; }
        public DbSet<Products> Product { get; set; }
        public DbSet<ShopCategory> ShopCategory { get; set; }
        public DbSet<Shops> Shop { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionCounter> TransactionCounter { get; set; }
        public DbSet<Wallet> Wallet { get; set; }

        //public DbSet<Chemical> Chemical { get; set; }
        //public DbSet<User> User { get; set; }
    }


}
