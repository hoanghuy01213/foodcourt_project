﻿using Application.Commons;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly DbSet<T> _dbSet;
        private readonly AppDbContext _appDBContext;

        public GenericRepository(AppDbContext appDBContext)
        {
            this._appDBContext = appDBContext;
            _dbSet = appDBContext.Set<T>();
        }
        public async Task AddEntityAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }
        public async Task<T> AddEntityAsyncGeneric(T obj)
        {
            await _dbSet.AddAsync(obj);
            return obj;
        }
        public async Task AddEntityRange(IEnumerable<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }
        public void UpdateEntity(T entity)
        {
            _dbSet.Update(entity);
        }
        public void UpdateRange(IEnumerable<T> entities)
        {
            _dbSet.UpdateRange(entities);
        }
        public void SoftRemoveEntity(T entity)
        {
            _dbSet.Remove(entity);
        }
        public void SoftRemoveEntityById(T entity)
        {
            _dbSet.Remove(entity);
        }
        public void SoftRemoveEntityRange(IEnumerable<T> entities)
        {
            _dbSet.RemoveRange(entities);
        }
        public async Task<IEnumerable<T>> ExecuteQueryAsync(string sqlQuery)
        {
            return await _dbSet.FromSqlRaw(sqlQuery).ToListAsync();
        }
        public async Task<IEnumerable<T>> GetListAsync()
        {
            var entities = await _dbSet.AsQueryable().ToListAsync();
            return entities;
        }
        public async Task<IEnumerable<T>> GetListAsync(bool isTracked = true)
        {
            if (isTracked)
            {
                List<T> cachedDatas = await _dbSet.ToListAsync();
                return cachedDatas;
            }
            else
            {
                List<T> cachesDatas = await _dbSet.AsNoTracking().ToListAsync();
                return cachesDatas;
            }
        }
        public async Task<IEnumerable<T>> GetListAsync(params string[] otherEntities)
        {
            IQueryable<T> entities = null;
            foreach (string other in otherEntities)
            {
                if (entities == null)
                {
                    entities = _dbSet.Include(other);
                }
                else
                {
                    entities = entities.Include(other);
                }
            }
            return await entities.ToListAsync();
        }
        public List<T> GetAllByCondition(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }
        public async Task<IEnumerable<T>>
            GetAllMultiIncludeAsync(
                Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                bool disableTracking = true)
        {
            IQueryable<T> query = _dbSet;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }
            query = include(query);
            return await query.ToListAsync();
        }
        public async Task<T> GetEntityByIdAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }
        public async Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
