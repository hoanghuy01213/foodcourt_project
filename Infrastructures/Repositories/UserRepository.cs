﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext appDBContext) : base(appDBContext)
        {
        }

        public Task<bool> CheckUserNameExited(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<User> GetUserByUserNameAndPasswordHash(string userName, string passwordHash)
        {
            throw new NotImplementedException();
        }

        //public UserRepository(AppDbContext dbContext,
        //    ICurrentTime timeService,
        //    IClaimsService claimsService)
        //    : base(dbContext,
        //          timeService,
        //          claimsService)
        //{
        //    _dbContext = dbContext;
        //}

        //public Task<bool> CheckUserNameExited(string userName) => _dbContext.Users.AnyAsync(u => u.UserName == userName);

        //public async Task<User> GetUserByUserNameAndPasswordHash(string userName, string passwordHash)
        //{
        //    var user = await _dbContext.Users
        //        .FirstOrDefaultAsync(record => record.UserName == userName
        //                                && record.PasswordHash == passwordHash);
        //    if (user is null)
        //    {
        //        throw new Exception("UserName & password is not correct");
        //    }

        //    return user;
        //}
    }
}
