﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class CardConfiguration : IEntityTypeConfiguration<Card>
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.CardType).WithMany(x=>x.Cards).HasForeignKey(x=>x.CardTypeId);
            builder.HasMany(x => x.Wallets).WithOne(x => x.Card).HasForeignKey(x => x.CardId);
        }
    }
}
