﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.TransactionCounter).WithMany(x => x.Transactions).HasForeignKey(x => x.TransactionCounterId);
            builder.HasOne(x => x.Wallet).WithMany(x => x.Transactions).HasForeignKey(x => x.WalletId).OnDelete(DeleteBehavior.SetNull);
            builder.HasOne(x => x.Orders).WithMany(x => x.Transactions).HasForeignKey(x => x.OrderId);
        }
    }
}
