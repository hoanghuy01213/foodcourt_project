using Infrastructures;
using FoodCourtAPI.Middlewares;
using FoodCourtAPI;
using Application.Commons;

var builder = WebApplication.CreateBuilder(args);

// parse the configuration in appsettings
var configuration = builder.Configuration;
builder.Services.AddInfrastructuresService(configuration);
builder.Services.AddWebAPIService();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<GlobalExceptionMiddleware>();
app.UseMiddleware<PerformaceMiddleware>();
app.MapHealthChecks("/healthchecks");
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
