﻿using Microsoft.AspNetCore.Mvc;

namespace FoodCourtAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        
    }
}
